FROM python:3.6.1-slim
ENV PYTHONUNBUFFERED 1
RUN mkdir /code && mkdir code/1 && mkdir code/2
WORKDIR /code/1
#COPY . /code/1
RUN pip install -r requirements.txt
# RUN python manage.py migrate
